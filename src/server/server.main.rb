#encoding: utf-8

require 'socket'
require 'rubyhelper'

require_relative 'server.log.rb'
require_relative 'server.client.rb'

class Server

  RETRY_NUMBER_MAX = 10

  def initialize listen="0.0.0.0", port=4202, modt="Welcome"
    @listen = listen
    @port = port
    @config = {}
    @modt = modt
    if (@s = TCPServer.new @listen, @port)
      log LOG_SERV, "Serveur running"
      trap("SIGINT") { log LOG_SERV, "[SIGINT] Serveur exit" ; exit(0) }
    else
      log LOG_SERV, "Serveur failed"
      raise :server_failed_to_run
    end
  end

  def start params=
    # Start an admin console
    # Thread.start{  }
    loop do
      Thread.start(@s.accept) do |client|
        try_number = 0
        begin
          try_number += 1
          handle_client(client)
        rescue
          log LOG_SERV, "client error"
          retry if try_number < RETRY_NUMBER_MAX
        end
        client.close
        log LOG_SERV, "client died"
      end
    end
  end

end
